#pragma once
#include <iostream>

class Vector3
{
public:
    Vector3 (double x, double y, double z);
    ~Vector3 ();

    double x_get() const;
    void x_set(double value);

    double y_get() const;
    void y_set(double value);

    double z_get() const;
    void z_set(double value);


    // Operations

    double distance(const Vector3& other) const;

    double dot_product(const Vector3& other) const;

    Vector3 operator+(const Vector3& other) const;

    Vector3 operator*(double x) const;

    Vector3 operator*(const Vector3& other) const;

    Vector3 normalize() const;

    Vector3 compute(const Vector3& other) const;

private:
    double x_;
    double y_;
    double z_;
};



std::ostream& operator<<(std::ostream& out, const Vector3& v);
