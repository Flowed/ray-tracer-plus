#pragma once

#include <iostream>
#include <fstream>
#include "scene.hh"


class Parser
{
public:
    Parser(std::ifstream& input);
    ~Parser();


    Scene& parse_scene();

private:

    void parse_line(std::string line, Scene& scene);

    std::ifstream& input_;
};
