#include "ascii.hh"


Ascii::Ascii(Screen& screen)
    : screen_{screen}
{
}

Ascii::~Ascii()
{}

void Ascii::print_screen()
{
    bool even = false;
    for (auto line : screen_.pixels_get())
    {
        even = !even;
        if (even)
            continue;

        for (auto pix : line)
            std::cout << ascii_compute(pix);
        std::cout << std::endl;
    }

}


char Ascii::ascii_compute(Color col)
{
    double r = col.r_get() * 255;
    double g = col.g_get() * 255;
    double b = col.b_get() * 255;
    if (r == 0 && g == 0 && b == 0)
        return ' ';
    else if (r + g + b < 50)
        return '.';
    else if (r + g + b < 100)
        return ',';
    else if (r + g + b < 125)
        return ':';
    else if (r + g + b < 150)
        return ';';
    else if (r + g + b < 200)
        return '!';
    else if (r + g + b < 250)
        return '$';
    else if (r + g + b < 300)
        return '%';
    else if (r + g + b < 350)
        return '#';
    else
        return '@';
}
