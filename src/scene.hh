#pragma once

#include <vector>
#include "camera.hh"
#include "object.hh"
#include "light.hh"
#include "screen.hh"

class Scene
{
public:

    static Scene& get_instance()
    {
        static Scene inst;
        return inst;
    }

    Scene& operator=(const Scene&) = delete;

    ~Scene() = default;

    Camera* cam_get() const;

    void cam_set(Camera* value);

    Screen* screen_get();

    void screen_set(Screen* value);

    void add_object(Object* obj);

    void add_light(Light* l);

    void compute();

private:
    Scene()
    {
        screen_ = nullptr;
        cam_ = nullptr;
    };

    Color compute_lights(const Rendered& obj_render, const Vector3& pos,
                         const Object& obj);

    Color compute_pixel(const Ray& ray);

    Screen* screen_;
    Camera* cam_;
    std::vector<Object*> obj_list_;
    std::vector<Light*> light_list_;
};
