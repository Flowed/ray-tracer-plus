#pragma once

#include "light.hh"
#include "color.hh"

class Directional : public Light
{
public:
    Directional(Color color, Vector3 direction);

    const Vector3& dir_get() const;

    void dir_set(const Vector3& value);

    virtual Color compute_value(const Rendered& obj_render, const Vector3& pos,
                                const Object& obj) override;

private:
    Vector3 dir_;

};
