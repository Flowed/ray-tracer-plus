#include "ambient.hh"

Ambient::Ambient(Color color)
    : Light(color)
{}




Color Ambient::compute_value(const Rendered& obj_render, const Vector3& pos,
                             const Object& obj)
{
    (void)pos;
    (void)obj;
    Color res(color_.r_get() * obj_render.color_get().r_get() * 0.1,
              color_.g_get() * obj_render.color_get().g_get() * 0.1,
              color_.b_get() * obj_render.color_get().b_get() * 0.1);
    return res;
}
