#pragma once

#include "light.hh"
#include "color.hh"

class Point : public Light
{
public:
    Point(Color color, Vector3 pos);

    const Vector3& pos_get() const;

    void pos_set(const Vector3& value);

    virtual Color compute_value(const Rendered& obj_render, const Vector3& pos,
                                const Object& obj) override;

private:
    Vector3 pos_;

};
