#include <cmath>
#include "point.hh"
#include "scene.hh"

Point::Point(Color color, Vector3 pos)
    : Light(color), pos_{pos}
{}


const Vector3& Point::pos_get() const
{
    return pos_;
}

void Point::pos_set(const Vector3& value)
{
    pos_ = value;
}



Color Point::compute_value(const Rendered& obj_render, const Vector3& pos,
                           const Object& obj)
{
    double distance = pos_.distance(pos);

    const Vector3& normal = obj.normal_vect(pos);

    double attenuation = 1 / distance;
    double diff = obj_render.diff_get();

    Vector3 light_dir = pos_.compute(pos);

    double Ld = diff * attenuation * light_dir.dot_product(normal);

    double r = obj_render.color_get().r_get() * color_.r_get() * Ld;
    double g = obj_render.color_get().g_get() * color_.g_get() * Ld;
    double b = obj_render.color_get().b_get() * color_.b_get() * Ld;

    Color res(r, g, b);
    res.positive();
    return res;
}

