#include "light.hh"


Color Light::color_get() const
{
    return color_;
}

void Light::color_set(const Color& value)
{
    color_ = value;
}


