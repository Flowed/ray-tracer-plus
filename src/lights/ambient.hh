#pragma once

#include "light.hh"
#include "color.hh"

class Ambient : public Light
{
public:
    Ambient(Color color);

    virtual Color compute_value(const Rendered& obj_render, const Vector3& pos,
                                const Object& obj) override;

};
