#include <iostream>
#include "screen.hh"
#include "color.hh"

// Converts a screen to an ascii representation
class Ascii
{
public:
    Ascii(Screen& screen);
    ~Ascii();

    void print_screen();

private:
    char ascii_compute(Color col);

    Screen& screen_;
};
