#include <cmath>
#include <map>
#include "scene.hh"
#include "ray.hh"

Camera* Scene::cam_get() const
{
    return cam_;
}

void Scene::cam_set(Camera* value)
{
    cam_ = value;
}


void Scene::screen_set(Screen* value)
{
    screen_ = value;
}

Screen* Scene::screen_get()
{
    return screen_;
}


void Scene::add_object(Object* obj)
{
    obj_list_.push_back(obj);
}

void Scene::add_light(Light* l)
{
    light_list_.push_back(l);
}



/* Computes the color of the pixel at pos according to the lights */
Color Scene::compute_lights(const Rendered& obj_render, const Vector3& pos, const Object& obj)
{
    Color res;
    for (auto light : light_list_)
    {
        res = res + light->compute_value(obj_render, pos, obj);
        if (res.r_get() > 1)
            res.r_set(1);
        if (res.g_get() > 1)
            res.g_set(1);
        if (res.b_get() > 1)
            res.b_set(1);
    }

    return res;
}



/* Computes the color of the pixel corresponding to the ray */
Color Scene::compute_pixel(const Ray& ray)
{

    std::map<double, std::pair<Object*, Rendered*>> colisions;
    for (auto obj : obj_list_)
    {
        double dist = 0;
        auto render = obj->intersect(ray, dist);
        if (dist)
            colisions[dist] = std::make_pair(obj, render);
    }

    if (!colisions.empty())
    {
        double col_distance = colisions.begin()->first;
        auto obj_info = colisions.begin()->second;
        Vector3 intersect = (ray.dir * col_distance) + ray.pos;
        return compute_lights(*obj_info.second, intersect, *obj_info.first);
    }
    else
        return Color(0, 0, 0);
}



/* Updates the screen to the current scene */
void Scene::compute()
{
    const double fov = 90 * 0.0174533; // in radians
    Vector3 cam_u = cam_->u_get().normalize();
    Vector3 cam_v = cam_->v_get().normalize();

    Vector3 w = cam_u * cam_v;
    double L = screen_->width_get() / (2 * std::tan(fov / 4)); // distance screen-camera
    Vector3 C = cam_->pos_get() + (w * L); // center of the screen

    // Browse each pixel of the screen
    for (unsigned j = 0 ; j < screen_->height_get(); j++)
    {
        for (unsigned i = 0; i < screen_->width_get() ; i++)
        {
            Vector3 screen_point(C.x_get() + i - screen_->width_get() / 2,
                                 C.y_get() + j - screen_->height_get() /2,
                                 C.z_get());

            Ray outgoing(cam_->pos_get(), cam_->pos_get().compute(screen_point));
            screen_->set_pixel(i, j, compute_pixel(outgoing));
        }
    }
}
