#include <vector>
#include <sstream>
#include "parser.hh"
#include "sphere.hh"
#include "plane.hh"
#include "color.hh"
#include "ambient.hh"
#include "directional.hh"
#include "point.hh"


Parser::Parser(std::ifstream& input)
    : input_(input)
{}


Scene& Parser::parse_scene()
{
    Scene& res = Scene::get_instance();

    std::string line;
    while(std::getline(input_, line))
        parse_line(line, res);

    if (res.screen_get() && res.cam_get())
        return res;
    else
        throw std::logic_error("Error: Could not instantiate camera from file");
}



void Parser::parse_line(std::string line, Scene& scene)
{

    // Split the string into a vector of string
    std::istringstream buffer(line);
    std::vector<std::string> split;
    while(buffer)
    {
        std::string tmp;
        buffer >> tmp;
        if (tmp != "")
            split.push_back(tmp);
    }

    // Fill the approriate field
    if (split[0] == "screen" && split.size() == 3)
    {
        unsigned resx = std::stod(split[1]);
        unsigned resy = std::stod(split[2]);
        scene.screen_set(new Screen(resx, resy));
    }
    else if (split[0] == "camera" && split.size() == 10)
    {
        Vector3 pos(std::stod(split[1]), std::stod(split[2]), std::stod(split[3]));
        Vector3 u(std::stod(split[4]), std::stod(split[5]), std::stod(split[6]));
        Vector3 v(std::stod(split[7]), std::stod(split[8]), std::stod(split[9]));
        Camera* cam = new Camera(pos, u, v);
        scene.cam_set(cam);
    }
    else if (split[0] == "sphere" && split.size() == 14)
    {
        Vector3 pos(std::stod(split[2]), std::stod(split[3]), std::stod(split[4]));
        Color color(std::stod(split[9]) / 255, std::stod(split[10]) / 255,
                    std::stod(split[11]) / 255,
                    std::stod(split[12]), std::stod(split[13]));
        Sphere* tmp_sphere = new Sphere(std::stod(split[1]), pos, std::stod(split[5]),
                                        std::stod(split[6]), std::stod(split[7]),
                                        std::stod(split[8]), color);
        scene.add_object(tmp_sphere);
    }
    else if (split[0] == "plane" && split.size() == 14)
    {
        Color color(std::stod(split[9]) / 255, std::stod(split[10]) / 255,
                    std::stod(split[11]) / 255,
                    std::stod(split[12]), std::stod(split[13]));
        Plane* tmp_plane = new Plane(std::stod(split[1]), std::stod(split[2]),
                                     std::stod(split[3]), std::stod(split[4]),
                                     Vector3(0, 0, 0), std::stod(split[5]),
                                     std::stod(split[6]), std::stod(split[7]),
                                     std::stod(split[8]), color);
        scene.add_object(tmp_plane);

    }
    else if (split[0] == "alight" && split.size() == 4)
    {
        Color color(std::stod(split[1]) / 255, std::stod(split[2]) / 255,
                    std::stod(split[3]) / 255);
        Ambient* tmp_ambient = new Ambient(color);
        scene.add_light(tmp_ambient);
    }
    else if (split[0] == "dlight" && split.size() == 7)
    {
        Vector3 dir(std::stod(split[1]), std::stod(split[2]), std::stod(split[3]));
        Color color(std::stod(split[4]) / 255, std::stod(split[5]) / 255,
                    std::stod(split[6]) / 255);
        Directional* tmp_directional = new Directional(color, dir);
        scene.add_light(tmp_directional);
    }
    else if (split[0] == "plight" && split.size() == 7)
    {
        Vector3 pos(std::stod(split[1]), std::stod(split[2]), std::stod(split[3]));
        Color color(std::stod(split[4]) / 255, std::stod(split[5]) / 255,
                    std::stod(split[6]) / 255);
        Point* tmp_point = new Point(color, pos);
        scene.add_light(tmp_point);
    }
    else
        std::cerr << "Warning: ignored invalid line: " << line << std::endl;
}




Parser::~Parser()
{}
