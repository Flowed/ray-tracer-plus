#include <iostream>
#include "screen.hh"


Screen::Screen(unsigned width, unsigned height)
    : width_{width}, height_{height}
{
    pixels_ = new color_matrix(height, std::vector<Color>(width));
}


Screen::~Screen()
{
    delete pixels_;
}


unsigned Screen::width_get() const
{
    return width_;
}

unsigned Screen::height_get() const
{
    return height_;
}


void Screen::width_set(unsigned value)
{
    width_ = value;
    delete pixels_;
    pixels_ = new color_matrix(height_, std::vector<Color>(width_));
}

void Screen::height_set(unsigned value)
{
    height_ = value;
    delete pixels_;
    pixels_ = new color_matrix(height_, std::vector<Color>(width_));
}


Screen::color_matrix& Screen::pixels_get()
{
    if (!pixels_)
        throw std::logic_error("Invalid access to null pixel matrix");
    return *pixels_;
}



void Screen::set_pixel(unsigned x, unsigned y, const Color& color) const
{
    (*pixels_)[y][x] = color;
}
