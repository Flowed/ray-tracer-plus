#pragma once

#include "color.hh"

class Rendered
{
public:
    Rendered(double diff, double refl, double spec, double shin, Color color);
    ~Rendered() = default;

    const Color& color_get() const
    {
        return color_;
    }

    double diff_get() const
    {
        return diff_;
    }

protected:
    double diff_;
    double refl_;
    double spec_;
    double shin_;
    Color color_;
};
