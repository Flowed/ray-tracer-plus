#include <cmath>
#include "color.hh"

Color::Color()
    : r_(0), g_(0), b_(0), refr_(1), opac_(1)
{}


Color::Color(double r, double g, double b)
    : r_(r), g_(g), b_(b), refr_(1), opac_(1)
{}


Color::Color(double r, double g, double b, double refr, double opac)
    : r_(r), g_(g), b_(b), refr_(refr), opac_(opac)
{}



double Color::r_get() const
{
    return r_;
}

double Color::g_get() const
{
    return g_;
}

double Color::b_get() const
{
    return b_;
}


void Color::r_set(double value)
{
    r_ = value;
}

void Color::g_set(double value)
{
    g_ = value;
}

void Color::b_set(double value)
{
    b_ = value;
}

void Color::positive()
{
    if (r_ < 0)
        r_ = 0;
    if (g_ < 0)
        g_ = 0;
    if (b_ < 0)
        b_ = 0;
}

Color& Color::operator=(const Color& other)
{
    r_ = other.r_;
    g_ = other.g_;
    b_ = other.b_;
    refr_ = other.refr_;
    opac_ = other.opac_;
    return *this;
}


Color Color::operator+(const Color& other) const
{
    Color res(r_ + other.r_, g_ + other.g_, b_ + other.b_);
    return res;
}

std::ostream& operator<<(std::ostream& out, const Color& c)
{
    out << std::round(c.r_get() * 255) << " "
        << std::round(c.g_get() * 255) << " "
        << std::round(c.b_get() * 255);
    return out;
}
