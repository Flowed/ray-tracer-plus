#pragma once

#include "object.hh"
#include "vector3.hh"

class Camera : public Object
{
public:
    Camera(Vector3 pos, Vector3 u, Vector3 v);
    ~Camera() = default;

    virtual Rendered* intersect(const Ray& ray, double& distance) override;

    virtual Vector3 normal_vect(const Vector3& pos) const override;

    Vector3 u_get() const;
    Vector3 v_get() const;

private:
    Vector3 u_;
    Vector3 v_;
};

