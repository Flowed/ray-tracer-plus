#include "object.hh"

Object::Object(Vector3 pos)
    : pos_(pos)
{}


const Vector3& Object::pos_get()
{
    return pos_;
}
