#include <cmath>
#include "plane.hh"

Plane::Plane(double a, double b, double c, double d, Vector3 pos, double diff,
        double refl, double spec, double shin, Color color)
    : Object(pos), Rendered(diff, refl, spec, shin, color)
{
    a_ = a;
    b_ = b;
    c_ = c;
    d_ = d;
}



Rendered* Plane::intersect(const Ray& ray, double& distance)
{
    Vector3 n(a_, b_, c_);
    n = n * -1;
    double denom  = n.dot_product(ray.dir);
    distance = 0;
    if (!denom)
        return nullptr;

    double t0 = ((n.dot_product(ray.pos)) + d_) / denom;
    if (t0 <= 0)
        return nullptr;

    distance = t0;
    return dynamic_cast<Rendered*>(this);
}


Vector3 Plane::normal_vect(const Vector3& pos) const
{
    return Vector3(a_, b_, c_);
}
